# [Dfrost 2012 Website](http://dfrost.in)

This is the code repository for Dfrost 2012, the annual design festival of NID R&D Campus, Bangalore.

The Dfrost 2012 website is build on top of HTML5 Boilerplate. We use a bit of PHP along with HTML, CSS & Javascript.
