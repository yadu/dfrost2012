<?php 
$site_current = "";
$sitePages = array("home","workshops","competitions","shows","schedule","pastevents","about","contact,register");
$css_path = "";

if (!empty($_GET['q'])) {
	$site_current = $_GET['q'];
} else {
	$site_current = "home";
}

if($site_current == "contact") {

require_once('recaptchalib.php');

}

/* are we online running locally? */	
$local = true;
$localList = array('localhost', '127.0.0.1');
if(!in_array($_SERVER['HTTP_HOST'], $localList)) {
	$local = false;
} else {
	$local = true;
}

if(!$local) { /* not in array */
	$test_path = $_SERVER['DOCUMENT_ROOT'] . "/data/" . $site_current . ".php";
	$css_path = $_SERVER['DOCUMENT_ROOT'] . "/css/";
} else {
	$test_path = $_SERVER['DOCUMENT_ROOT'] . "dfrost2012/data/" . $site_current . ".php";
	$css_path = $_SERVER['DOCUMENT_ROOT'] . "dfrost2012/css/";
}

?>

<?php include("header.php"); ?>

<?php include("sidebar.php"); ?>

<?php

if(file_exists($test_path)) {
	include($test_path);
} else {
	if(in_array($site_current, $sitePages)) {
		include("data/comingSoon.php");
	} else {
		include("404.html");
	}
}

?>
		
<?php include("footer.php"); ?>
