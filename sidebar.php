<div id="sidebarWrap" class="dropshadow">		
	<div id="sidebar">
		<div id="logo">
		<a href="http://dfrost.in" title="Dfrost 2012"><img src="img/logo.png" title="Dfrost 2012" alt="Dfrost 2012" border="0" /></a>
		</div>
		<hr class="sidebarSep" />
		<div id="navigation">
			<ul class="menuGroup">
				<li class="home"><a href="home">Superheroes</a></li>
				<li class="workshops"><a href="workshops">Workshops</a></li>
				<li class="competitions"><a href="competitions">Competitions</a></li>
				<li class="shows"><a href="shows">Shows</a></li>
				<li class="online"><a href="online">Specials!!</a></li>
			</ul>
			<ul class="menuGroup">
				<li class="sponsors"><a href="sponsors">Sponsors</a></li>
				<li class="schedule"><a href="schedule">Schedule</a></li>
				<li class="pastevents"><a href="pastevents">Past Events</a></li>
			</ul>
			<ul class="menuGroup">
				<li class="about"><a href="about">About</a></li>
				<li class="contact"><a href="contact">Contact</a></li>
				<li class="rsvp" style="display:none;"><a href="rsvp" target="_blank">R.S.V.P NOW!!</a></li>
			</ul>
		</div>
		<hr class="sidebarSep" />
		<div id="social">
			<a href="http://facebook.com/DfrostNID" title="Find us on Facebook." target="_blank"><img src="img/facebook.png" alt="fb" /></a>
		</div>
	</div>
</div>