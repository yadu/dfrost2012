$(document).ready(function(){				

	try {
			$('#banner').bjqs({
				// Width + Height used to ensure consistency
				width: 750,
				height: 500,
				// The type of animation (accespts slide or fade)
				animation: 'fade',
				// The duration in ms of the transition between slides
				animationDuration: 450,
				// Automatically rotate through the slides
				automatic: true,
				// Delay in ms between auto rotation of the slides
				rotationSpeed: 4000,
				// Pause the slider when any elements receive a hover event
				hoverPause: true,
				// Show the manual slider controls
				showControls: false,
				// Center the controls vertically
				centerControls: false,
				// Text to display in next/prev buttons (can accept html)
				nextText: 'Next',
				prevText: 'Prev',
				// Show positional markers
				showMarkers: true,
				// Center the positional indicators
				centerMarkers: false,
				// Allow navigation with arrow keys
				keyboardNav: true,
				// Use image title text as caption
				useCaptions: true
			});
		} catch(err) {
		// die silent
		}
		
		try {
			$(".fancybox").fancybox({
					openEffect  : 'none',
					closeEffect : 'none',

					prevEffect : 'none',
					nextEffect : 'none',

					closeBtn  : false,

					helpers : {
						title : {
							type : 'inside'
						},
						buttons	: {}
					},

					afterLoad : function() {
						this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
					}
				});
		} catch(err) {
		// die silent
		}
});