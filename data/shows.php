<div id ="wrap">
	<div id="content">
		<div id="mainContent">
		<div class="show">
			<h2>PARVAAZ</h2>
			<div class="meta">Day 2 - 16th Sept, 17.30 onwards</div>
			<h3>Parvaaz is a Bangalore-based rock band comprising of<br />
	Khalid Ahmed (vocals/guitars/song writer),<br />
	Kashif Iqbal (lead guitar/vocals/various instruments/lyrics),<br />
	Fidel Dsouza (bass) and<br />
	Sachin Banandur (drums & percussion).</h3>
	<p>Their unique sound is a combination of psychedelic blues with Kashmiri and Urdu lyrics. They shot to fame after their epic performance at The Great Indian October Fest and The Fireflies Festival Of Music held in Febuary 2011 at Bangalore. The band has recently released their debut E.P, "Behosh".</p>
	<p>
<img src="img/shows-parvaaz.png" alt="Parvaaz!" />
</p>
<iframe width="400" height="100" style="position: relative; display: block; width: 400px; height: 100px;" src="http://bandcamp.com/EmbeddedPlayer/v=2/album=2345831780/size=venti/bgcol=FFFFFF/linkcol=255a75/transparent=true/" allowtransparency="true" frameborder="0"><a href="http://parvaaz.bandcamp.com/album/behosh-e-p" target="_blank">Behosh E.P by PARVAAZ</a></iframe>
<div class="info">
<a href="https://www.facebook.com/parvaazmusic" target="_blank">facebook</a> | <a href="http://www.parvaaz.net/" target="_blank">website</a>
</div>
		</div>
		<div class="show">
			<h2>Theatre Group</h2>
			<div class="meta">Day 1 - 15th Sept, 17.30 onwards</div>
			<h3>Even superheros need a break!<br />
After a fun-filled day, unwind and enjoy short skits and<br />
performances (by students from NID Bangalore) and<br />
refresh for the next day at Dfrost.</h3>
	<p>
	<img src="img/shows-theatre.png" title="Parvaaz" />
	</p>
</div>
		</div>
	</div>
</div>
