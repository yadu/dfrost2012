<div id ="wrap">
	<div id="content">
		<div id="mainContent">
			<h1>At dfrost, we don't believe in sending people back empty handed. </h1>
			<h2>The competitions list includes -</h2>
			<div class="event">
			<h3>Snap shorts</h3>
			<div class="meta">Day 1 - 15th Sept, 10.30 onwards</div>
			<div class="meta_2">Free Registration | Prizes worth &#8377;8000</div>
			<div class="data">
			<p>Are you an amateur director / filmmaker? If yes, you would not want to miss this chance. Snap shorts is a unique competition where you need to race against time and create an interesting short film on a particular theme. This is one intense competition, which you absolutely cannot miss!</p>
<p>All videos will be shot with camera phones. Participants need to get their own camera phones and laptops for editing.</p>
			</div>
			</div>
			
			<div class="event">
			<h3>Pot - Pourri</h3>
			<div class="meta">Day 1 - 15th Sept, 15.30 - 18.30</div>
			<div class="meta_2">Free Registration | Win exciting prizes</div>
			<div class="data">
<p>Not every person is good at everything, even superheroes form teams to take down a supervillain. Hence here's your chance to team up with your friends and form you own super team and face the various challenges thrown at you during the Pot Pourri.</p>
<p>
<strong>Rules:</strong>
<ul>
<li>Team of 3.</li>
<li>Written Prelims, Stage Finals.</li>
<li>Will involve word games, pictionary, quiz, dumb charades, etc.</li>
</ul>
</p>
			</div>
			</div>
			
			<div class="event">
			<h3>T-shirt painting</h3>
			<div class="meta">Day 1 - 15th Sept, 15.30 - 17.30</div>
			<div class="meta_2">Free Registration | 30 Seats | Win exciting prizes</div>
			<div class="data">
			<p>We give you the chance to transform a plain Tee shirt into an exciting and cool garment. Let the designer in you take control.</p>
<p>
We will provide the Tees, paints, brush and any other stationary required for the competition. You just need to get your creative creative caps along!
</p>
			</div>
			</div>
			
			<div class="event">
			<h3>Band Wars</h3>
			<div class="meta">Day 2 - 16th Sept, 17.30 onwards</div>
			<div class="meta_2">Free Registration | Prizes worth &#8377;20000</div>
			<div class="data">
			<p>Wanna know if your band is 'groovy'? Curios if your latest 'jam' track is 'really cool'? Dare to test how tight your band really is? Bring 'em along for the battle.</p>
<p>
All bands need to bring their own instruments. Only the audio equipment and drumkit will be provided by us. Each band will get 20 minutes for sound check and the actual gig.</p>
			</div>
			</div>
			
		</div>
	</div>
</div>
