<div id ="wrap">
	<div id="content">
		<div id="mainContent">
			<div class="event">
			<h2>dfrost 2011</h2>
			<h3>Last year's Dfrost theme was 'Order in Chaos' and the<br />
	fest consisted of 2 exciting days of workshops,<br />
	competitions and other cultural shows.</h3>
	<div class="imgset">
	<a  class="fancybox" data-fancybox-group="dfrost2011" href="img/dfrost2011/dfrost01.jpg"><img src="img/dfrost2011/thumbs/dfrost01.jpg" alt="Dfrost 2011" width="240px" height="160px" border="0" /></a>
	<a class="fancybox" data-fancybox-group="dfrost2011" href="img/dfrost2011/dfrost02.jpg"><img src="img/dfrost2011/thumbs/dfrost02.jpg" alt="Dfrost 2011" width="240px" height="160px" border="0" /></a>
	<a class="fancybox" data-fancybox-group="dfrost2011" href="img/dfrost2011/dfrost03.jpg"><img src="img/dfrost2011/thumbs/dfrost03.jpg" alt="Dfrost 2011" width="240px" height="160px" border="0" /></a>
	</div>
	<div class="imgset">
	<a class="fancybox" data-fancybox-group="dfrost2011" href="img/dfrost2011/dfrost04.jpg"><img src="img/dfrost2011/thumbs/dfrost04.jpg" alt="Dfrost 2011" width="240px" height="160px" border="0" /></a>
	<a class="fancybox" data-fancybox-group="dfrost2011" href="img/dfrost2011/dfrost05.jpg"><img src="img/dfrost2011/thumbs/dfrost05.jpg" alt="Dfrost 2011" width="240px" height="160px" border="0" /></a>
	<a class="fancybox" data-fancybox-group="dfrost2011" href="img/dfrost2011/dfrost06.jpg"><img src="img/dfrost2011/thumbs/dfrost06.jpg" alt="Dfrost 2011" width="240px" height="160px" border="0" /></a>
	</div>
			</div>
			<div class="event">
			<h2>dfrost 2007 & 2009</h2>
			<h3>The first two episodes of dfrost consisted of exciting<br />
workshops and a rocking performance by Swarathma<br />
on both occasions. </h3>
	<div class="imgset">
	<a  class="fancybox" data-fancybox-group="dfrost2009" href="img/dfrost2009/dfrost01.jpg"><img src="img/dfrost2009/thumbs/dfrost01.jpg" alt="Dfrost 2009" width="240px" height="160px" border="0" /></a>
	<a class="fancybox" data-fancybox-group="dfrost2009" href="img/dfrost2009/dfrost02.jpg"><img src="img/dfrost2009/thumbs/dfrost02.jpg" alt="Dfrost 2009" width="240px" height="160px" border="0" /></a>
	<a class="fancybox" data-fancybox-group="dfrost2009" href="img/dfrost2009/dfrost03.jpg"><img src="img/dfrost2009/thumbs/dfrost03.jpg" alt="Dfrost 2009" width="240px" height="160px" border="0" /></a>
	</div>
			</div>

		</div>
	</div>
</div>
