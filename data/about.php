<div id ="wrap">
	<div id="content">
		<div id="mainContent">
		<h2>About NID</h2>
		<p>NID was established by the Government of India with a great foresight to cater to the need of training in design.<p>
<p>Since 1961, NID has been maintaining and setting new standards for its products, services and quality of education.</p>
<h2>About NID R & D Campus Bangalore</h2>
<p>In March 2006, the Research & Development campus in Bangalore was inaugurated, to foster a creative design spirit, identify new frontiers in the field of design, and provide innovative solutions to common problems in the present context. There are three research-intensive post-graduate programmes offered in the campus - Design for Digital Experience, Design for Retail Experience and Information & Interface Design.</p>
<h2>About Dfrost</h2>
<p>Dfrost is the annual design festival hosted by the National Institute of Design R&D campus,  Bangalore. The fest provides a wonderful opportunity to foster the understanding of design and its relevance in today's world, through workshops, competitions and display of students' works. By combining fun,creativity and awareness of design-oriented activities, dfrost aims to highlight the impact of design on society and people. It is a common platform for students from varied disciplines to showcase their potential and interact with professionals from diverse backgrounds.</p>
<p>
This year, dfrost will be hosted on 15th and 16th September, centered around the inspiring theme of superheroes to highlight the 'heroic' contributions of designers to society. </p>
		</div>
	</div>
</div>
