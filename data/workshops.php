<div id ="wrap">
	<div id="content">
		<div id="mainContent">
		<h2>Dfrost offers an exciting range of workshops for all participants.</h2>
		<h3>This year's exciting list includes -</h3>
		<div class="workshopItem">
		<h3 class="title">Pencil Jam</h3>
		<div class="datetime">Day 1 - 15th Sept, 10.30 - 16.30</div>
		<div class="info">&#8377;200 for students | &#8377;400 for professionals | 30 seats</div>
		<p>Are designers really superheroes? George Supreeth certainly seems to think so. Designers have been so hung up on visual design that the true value that they offer is often eclipsed by subjective interpretations of form, colour and space. Join Supreeth for an engaging session on how designers can uncover their true potential and bring real solutions to the problems that they engage with. Understand different systems of design thinking and how thought games can pivot problems into life changing solutions.</p>
<p>Learn to design like a Superhero!
</p>
<a class="linkSpecial registerLink" href="register" title="Register for pencil Jam">Register</a>
		</div>
		
		<div class="workshopItem">
		<h3 class="title">Pottery</h3>
		<div class="datetime">Day 1 - 15th Sept, 10.30 - 16.30</div>
		<div class="info">&#8377;300 | 30 seats</div>
		<p>Come get your hands dirty and make your own Eco friendly Ganesha. Want to try your hand at the potter's wheel? We have that too... Let's make it a potter's day out!! So come and join us for 2 hours of non stop fun and learning.</p>
<a class="linkSpecial registerLink" href="register" title="Register for pencil Jam">Register</a>
		</div>
		
		<div class="workshopItem">
		<h3 class="title">Prototyping</h3>
		<div class="datetime">Day 2 - 16th Sept, 10.30 - 15.30</div>
		<div class="info">&#8377;200 for students | &#8377;400 for professionals | 20 seats</div>
		<p>Often people have great ideas, but they are clueless about how to reproduce it for others to see. This workshop will teach you basics of prototyping and will allow you to present your concept visually. Uday Shankar is UX/front end architect who specializes in prototyping. Last year, he conducted a workshop in Dfrost, which proved to be so popular that we just couldn't think of excluding it.</p>
<p>Prototyping allows you to explore alternate design possibilities, test initial designs and make corrections. Most of the times, the technical aspects leave many people intimidated. We hope that this workshop can lay all your fears to rest.</p>
<p>We urge everyone to participate and develop this unique skill!</p>
<span class="registerLink" style="background-color:#ff0000;font-family: ALLER_BDIT;color: white !important;text-decoration: none;padding: 6px;margin: 0;display: inline-block;" title="SOLD OUT!">SOLD OUT!!</span>
		</div>
		
		<div class="workshopItem">
		<h3 class="title">Bamboozled</h3>
		<div class="datetime">Day 2 - 16th Sept, 10.30 - 17.30</div>
		<div class="info">&#8377;250 for students | &#8377;400 for professionals | 20 seats</div>
		<p>At NID, we believe in learning new and traditional crafts and now you have the opportunity to do the same. Get a hands-on experience in Bamboo craft from our expert faculty members. In this workshop, you will be taught Bamboo construction and joinery techniques.</p><p>
You can make cool lampshades, pen holders, art pieces , key chains and what not. The possibilities are endless. It is a rare chance for you to toy with interesting materials.</p>
<p>Don't miss the chance to brag about your own handcrafted items!</p>
<span class="registerLink" style="background-color:#ff0000;font-family: ALLER_BDIT;color: white !important;text-decoration: none;padding: 6px;margin: 0;display: inline-block;" title="SOLD OUT!">SOLD OUT!!</span>
		</div>
		
		<div class="workshopItem">
		<h3 class="title">Terracotta</h3>
		<div class="datetime">Day 2 - 16th Sept, 10.30 - 18.30</div>
		<div class="info">&#8377;250 for students | &#8377;400 for professionals | 25 seats</div>
		<p>Don't miss the chance to learn from acclaimed sculptor G Reghu! You will get the opportunity to acquire the skills and technique needed to create ceramic and terracotta art. This is a fun workshop where you can get really creative..!</p>
<a class="linkSpecial registerLink" href="register" title="Register for pencil Jam">Register</a>
		</div>
	</div>
</div>
