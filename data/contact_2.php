 <?php
 
$message = "";
$error = "";
$uname = "";
$email = "";
$status = "";

if(isset($_POST['submit'])) {

	if(isset($_POST['uname'])) {
		$uname = $_POST['uname'];
	}
	if(isset($_POST['email'])) {
		$email = $_POST['email'];
	}
	if(isset($_POST['message'])) {
		$message = $_POST['message'];
	}
	 
	if($uname == "" && $email == "" && $message == "") {
		$error = "Please fill in all required fields!";
	} else {
		$privatekey = "6LfGG9YSAAAAAIRujwwGkQ-1h2aG9Ke4cTV9rOTE";
		$resp = recaptcha_check_answer ($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

		if (!$resp->is_valid) {
		// What happens when the CAPTCHA was entered incorrectly
			$error = "The reCAPTCHA wasn't entered correctly.";
			$status = "";
		} else {
		// Your code here to handle a successful verification
		// send email
		
		$to      = 'dfrost@nid.edu';
		$subject = '[Dfrost.in Contact Form]';
		$msg = wordwrap("Name: ". $uname . "\nEmail: " . $email . "\nMessage: " . $message);

			if(mail($to, $subject, $msg)) {
				$error = "";
				$status = "Your message was sent, we will get in touch with you soon";
			} else {
				$error = "An error occured while sending the form, please try again after some time.";
				$status = " ";
			}
		}
	}
}

  ?>

<div id ="wrap">
	<div id="content">
		<div id="mainContent">
		<h1>Have a query? drop us a note :)</h1>
		<div id="info" class="clearfix">

 <script type="text/javascript">
 var RecaptchaOptions = {
    theme : 'red'
 };
 </script>
		
		<?php if($status == "") { ?>
			<div id="infoMailerForm">
			<?php
			if($error != "") {
				print "<div class=\"msgError\">";
				print $error;
				print "</div>";
			}
			?>
			<form method="POST" action="contact">
			<div class="formitem"><label for="txtuname">Name*</label><input name="uname" id="txtuname" type="text" value="<?php print $uname; ?>" class="required" /><br /></div>
			<div class="formitem"><label for="txtemail">Email*</label><input name="email" type="text" id="txtemail" value="<?php print $email; ?>" class="required" /></div>
			<div><label for="txtmessage">Message*</label><br /><textarea name="message" id="txtmessage" type="text" class="required"><?php print $message; ?></textarea></div>
			<div id="captcha">
			<?php
				$publickey = "6LfGG9YSAAAAAKRcfcGZkuJw5rDw21azyCKcJLK4";
				echo recaptcha_get_html($publickey);
			?>
			</div>
			<div id="btnSend"><input type="submit" name="submit" value="Send" /></div>
			</form>
			</div>
		<?php } else { ?>
			<div id="infoMailerForm">
				<?php print $status; ?>
			</div>
		<?php } ?>
		<div id="address">
		<p>
		<a href='&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#100;&#102;&#114;&#111;&#115;&#116;&#64;&#110;&#105;&#100;&#46;&#101;&#100;&#117;'>&#100;&#102;&#114;&#111;&#115;&#116;&#64;&#110;&#105;&#100;&#46;&#101;&#100;&#117;</a><br />
		+91 9591353636</p>
		<p>#12 HMT Link Road, Off Tumkur Road<br />Bangalore - 560 022</p>
		</div>
		</div>
		<h2>Location</h2>
<iframe id="map" width="792" height="426" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/maps?ie=UTF8&amp;cid=1353057478014026993&amp;q=NID+Bangalore+Teastall&amp;gl=IN&amp;hl=en&amp;t=m&amp;ll=13.036377,77.53408&amp;spn=0.008905,0.016072&amp;z=16&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="https://maps.google.co.in/maps?ie=UTF8&amp;cid=1353057478014026993&amp;q=NID+Bangalore+Teastall&amp;gl=IN&amp;hl=en&amp;t=m&amp;ll=13.036377,77.53408&amp;spn=0.008905,0.016072&amp;z=16&amp;iwloc=A&amp;source=embed" style="color:#0000FF;text-align:left">View Larger Map</a></small>
<p>
<a href="http://nid.edu" target="_blank"><img src="img/nid-logo.png" alt="National Institute of Design, Bangalore" border="0" /></a>
</p>
		</div>
	</div>
</div>
